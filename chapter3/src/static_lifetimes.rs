#[allow(dead_code)]
static N: i32 = 15;
#[allow(dead_code)]
static mut M: i32 = 20;

#[allow(dead_code)]
fn get_stat() -> &'static i32 {
    &N
}

#[allow(dead_code)]
fn stat_str() -> &'static str {
    "hello"
}

#[allow(dead_code)]
fn add_stat(n: i32) -> i32 {
    unsafe {
        M += n;
        M
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn get_stat_works() {
        let n = get_stat();
        assert_eq!(*n, 15);
    }

    #[test]
    fn stat_str_works() {
        let s = stat_str();
        assert_eq!(s, "hello");
    }

    #[test]
    fn stat_add_5() {
        let m = add_stat(5);
        assert_eq!(m, 25);
    }
}
