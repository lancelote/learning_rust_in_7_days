#[allow(dead_code)]
enum Tree<V> {
    Empty,
    Head(V, Box<Tree<V>>, Box<Tree<V>>),
}

use self::Tree::*;

impl<V: PartialOrd> Tree<V> {
    #[allow(dead_code)]
    fn empty() -> Self {
        Empty
    }

    #[allow(dead_code)]
    fn new(value: V) -> Self {
        Head(value, Box::new(Empty), Box::new(Empty))
    }

    #[allow(dead_code)]
    fn add(&mut self, value: V) {
        match self {
            Empty => {
                *self = Tree::new(value);
            }
            Head(data, left, right) => {
                if value < *data {
                    left.add(value);
                } else {
                    right.add(value);
                }
            }
        }
    }
}

impl<T: Clone> Tree<T> {
    #[allow(dead_code)]
    fn values(&self) -> Vec<T> {
        match self {
            Empty => vec![],
            Head(data, left, right) => {
                let mut res = left.values();
                res.push(data.clone());
                res.append(&mut right.values());
                res
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let mut tree = Tree::new(5);
        tree.add(4);
        tree.add(3);
        tree.add(9);

        assert_eq!(tree.values(), vec![3, 4, 5, 9]);
    }
}
