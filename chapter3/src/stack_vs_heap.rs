#[allow(dead_code)]
#[derive(Debug, PartialEq)]
enum LinkedList<T> {
    Tail,
    Head(T, Box<LinkedList<T>>),
}

use self::LinkedList::*;

#[allow(dead_code)]
impl<T> LinkedList<T> {
    fn empty() -> Self {
        Tail
    }

    fn new(value: T) -> Self {
        Head(value, Box::new(Tail))
    }

    fn push(self, value: T) -> Self {
        Head(value, Box::new(self))
    }

    fn push_back(&mut self, value: T) {
        match self {
            Tail => {
                *self = LinkedList::new(value);
            }
            Head(_, list) => {
                list.push_back(value);
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn push_item() {
        let list = LinkedList::new(3);
        let list = list.push(4);
        assert_eq!(list, Head(4, Box::new(Head(3, Box::new(Tail)))));
    }

    #[test]
    fn empty_check() {
        let list: LinkedList<i32> = LinkedList::empty();
        assert_eq!(list, Tail);
    }

    #[test]
    fn push_back_works() {
        let mut list = LinkedList::new(3);
        list = list.push(4);
        list.push_back(2);

        assert_eq!(
            list,
            Head(4, Box::new(Head(3, Box::new(Head(2, Box::new(Tail))))))
        );
    }
}
