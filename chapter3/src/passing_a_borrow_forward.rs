#[allow(dead_code)]
#[allow(clippy::needless_lifetimes)]
fn trim_left<'a>(s: &'a str) -> &'a str {
    for (i, c) in s.char_indices() {
        if c == ' ' {
            continue;
        }
        return s.get(i..s.len()).unwrap();
    }

    ""
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn trim_left_works() {
        assert_eq!(trim_left("   hello"), "hello");
    }

    #[test]
    fn trim_left_works_on_string() {
        let s = "  hello".to_string();
        assert_eq!(trim_left(s.as_str()), "hello");
    }

    #[test]
    fn trim_left_with_mutability() {
        let mut s = "    hello".to_string();
        {
            let s2 = trim_left(&s);
            assert_eq!(s2, "hello");
        }
        s.push_str(" world");
        assert_eq!(trim_left(s.as_str()), "hello world");
    }
}
