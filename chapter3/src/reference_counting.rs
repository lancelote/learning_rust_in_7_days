use std::cell::RefCell;
use std::rc::Rc;

#[allow(dead_code)]
pub fn make_rc(i: i32) -> Rc<RefCell<i32>> {
    let a = Rc::new(RefCell::new(i));
    let b = a.clone();

    {
        let m = &mut *a.borrow_mut();
        *m += 2;
    }

    {
        let n = &mut *a.borrow_mut();
        *n += 1;
    }

    b
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn correct_rc() {
        let r = make_rc(5);
        assert_eq!(*r.borrow(), 8);
    }
}
