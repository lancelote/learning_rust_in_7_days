#[derive(Debug, PartialEq)]
struct GBP(i32);

// fn money_pointer<'a>(i: i32) -> &'a GBP {
//     let gbp = GBP(i);
//     &gbp
// }

#[allow(dead_code)]
fn on_money(value: i32, other: i32) -> GBP {
    let gbp = GBP(value);
    let pointer = &gbp;
    GBP(pointer.0 + other)
}

#[cfg(test)]
mod tests {
    use crate::lifetimes::*;

    // #[test]
    // fn money_pointer_works_ok() {
    //     let gbp = money_pointer(3);
    //     assert_eq!(*gbp, GBP(3));
    // }

    #[test]
    fn on_money_works_ok() {
        let gbp = on_money(3, 4);
        assert_eq!(gbp, GBP(7));
    }
}
