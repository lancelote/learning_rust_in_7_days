mod accepting_generic_parameters;
mod defining_a_trait;
pub mod error_handling_with_traits;
mod generic_iterator;
mod generic_structs;
mod parse;
mod useful_traits;
