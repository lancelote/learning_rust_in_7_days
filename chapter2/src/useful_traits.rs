use std::fmt;

#[derive(Debug, PartialEq)]
pub struct USD(i32);

impl fmt::Display for USD {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let r = (self.0 as f32) / 100.;
        if r < 0. {
            write!(f, "-${:.2}", -r)
        } else {
            write!(f, "${:.2}", r)
        }
    }
}

impl Clone for USD {
    fn clone(&self) -> Self {
        USD(self.0)
    }
}

impl Copy for USD {}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn usd_display() {
        let usd = USD(230);
        assert_eq!(usd.to_string(), String::from("$2.30"));
    }

    #[test]
    fn usd_clone() {
        let usd = USD(230);
        #[allow(clippy::clone_on_copy)]
        let new_usd = usd.clone();
        assert_eq!(usd, new_usd);
    }

    #[test]
    fn usd_copy() {
        let usd = USD(230);
        let new_usd = usd;
        assert_eq!(usd, new_usd);
    }
}
