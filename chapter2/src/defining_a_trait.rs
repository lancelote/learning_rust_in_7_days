#[derive(Debug, PartialEq)]
struct USD(i32);

#[derive(Debug, PartialEq)]
struct GBP(i32);

#[derive(Debug, PartialEq)]
struct CAD(i32);

trait ToUSD {
    fn to_usd(&self) -> USD;

    fn convert<T: FromUSD>(&self) -> T {
        T::from_usd(&self.to_usd())
    }
}

impl ToUSD for GBP {
    fn to_usd(&self) -> USD {
        USD((self.0 * 130) / 100)
    }
}

trait FromUSD {
    fn from_usd(u: &USD) -> Self;
}

impl FromUSD for CAD {
    fn from_usd(u: &USD) -> Self {
        CAD((u.0 * 130) / 100)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn convert_gbp_to_usd() {
        let gbp = GBP(200);
        let usd = gbp.to_usd();
        assert_eq!(usd, USD(260));
    }

    #[test]
    fn cad_from_usd() {
        let usd = GBP(200).to_usd();
        let cad = CAD::from_usd(&usd);
        assert_eq!(cad, CAD(338));
    }

    #[test]
    fn convert() {
        let cad: CAD = GBP(200).convert();
        assert_eq!(cad, CAD(338));
    }
}
