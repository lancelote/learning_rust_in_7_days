use rand::Rng;
use std::ops::Add;

#[derive(Debug)]
struct Point {
    x: i32,
    y: i32,
}

impl Add for Point {
    type Output = Point;

    fn add(self, rhs: Self) -> Self::Output {
        Point {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl Point {
    fn random() -> Self {
        let mut rng = rand::thread_rng();
        Point {
            x: rng.gen(),
            y: rng.gen(),
        }
    }
}

fn main() {
    let point = Point::random();
    println!("{:?}", point);
}
