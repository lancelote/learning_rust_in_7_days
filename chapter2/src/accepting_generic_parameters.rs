#[derive(Debug, PartialEq)]
struct USD(i32);

#[derive(Debug, PartialEq)]
struct GBP(i32);

#[derive(Debug, PartialEq)]
struct CAD(i32);

trait ToUSDValue<F> {
    fn to_usd_value(&self, currency: F) -> f32;
}

trait FromUSDValue<F> {
    fn from_usd_value(&self, value: f32) -> F;
}

struct Exchange {
    cad: f32,
    gbp: f32,
}

impl ToUSDValue<GBP> for Exchange {
    fn to_usd_value(&self, currency: GBP) -> f32 {
        (currency.0 as f32) * self.gbp
    }
}

impl FromUSDValue<CAD> for Exchange {
    fn from_usd_value(&self, value: f32) -> CAD {
        CAD((value / self.cad) as i32)
    }
}

trait ExchangeType<F, T> {
    fn convert(&self, currency: F) -> T;
}

impl<E, F, T> ExchangeType<F, T> for E
where
    E: ToUSDValue<F> + FromUSDValue<T>,
{
    fn convert(&self, currency: F) -> T {
        self.from_usd_value(self.to_usd_value(currency))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn exchange_works() {
        let gbp = GBP(200);
        let exchange = Exchange { cad: 0.7, gbp: 1.3 };
        let cad: CAD = exchange.convert(gbp);
        assert_eq!(cad, CAD(371));
    }
}
