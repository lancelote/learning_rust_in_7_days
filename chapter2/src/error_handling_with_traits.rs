//! A library for handling money

use super::parse::*;
use std::str::FromStr;

#[derive(Debug, PartialEq)]
pub enum GBPError {
    ParseError(ParseMoneyError),
}

impl From<ParseMoneyError> for GBPError {
    fn from(p: ParseMoneyError) -> Self {
        GBPError::ParseError(p)
    }
}

/// Parse your money from a string
/// ```
/// use self::chapter2::error_handling_with_traits::GBP;
/// let gbp = "£32.45".parse();
/// assert_eq!(gbp, Ok(GBP(3245)));
/// ```
#[derive(Debug, PartialEq)]
pub struct GBP(pub i32);

impl FromStr for GBP {
    type Err = GBPError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(GBP(parse_sym_money(s, '£', 2)?))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn gbp_from_str() {
        let gbp = "£32.45".parse();
        assert_eq!(gbp, Ok(GBP(3245)));
    }
}
