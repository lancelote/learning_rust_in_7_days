use std::ops::AddAssign;

struct Stepper<T> {
    current: T,
    step: T,
    stop_at: T,
}

impl<T> Stepper<T> {
    #[allow(dead_code)]
    fn new(start: T, stop: T, step: T) -> Self {
        Stepper {
            current: start,
            step,
            stop_at: stop,
        }
    }
}

impl<T> Iterator for Stepper<T>
where
    T: AddAssign + Copy + PartialOrd,
{
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        if self.current >= self.stop_at {
            return None;
        }
        let res = self.current;
        self.current += self.step;
        Some(res)
    }
}

#[allow(dead_code)]
fn sum_list<I, S>(list: I, mut s: S) -> S
where
    I: IntoIterator<Item = S>,
    S: AddAssign,
{
    for n in list {
        s += n;
    }
    s
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn stepper_works() {
        let mut c = 0;
        for n in Stepper::new(2, 10, 2) {
            c += n;
        }

        assert_eq!(c, 20);
    }

    #[test]
    fn sum_list_works() {
        let sl = sum_list(Stepper::new(3, 10, 2), 0);
        assert_eq!(sl, 24);
    }

    #[test]
    fn fold_works_on_stepper() {
        let fl: i32 = Stepper::new(4, 10, 2).sum();
        assert_eq!(fl, 18);
    }
}
