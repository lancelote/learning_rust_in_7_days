#[derive(Debug, PartialEq, Clone)]
struct USD(i32);

#[derive(Debug, PartialEq, Clone)]
struct GBP(i32);

#[derive(Debug, PartialEq, Clone)]
struct CAD(i32);

trait ToUSDValue<F> {
    fn to_usd_value(&self, currency: F) -> f32;
}

trait FromUSDValue<F> {
    fn from_usd_value(&self, value: f32) -> F;
}

struct Ex {
    account_id: i32,
    cad: f32,
    gbp: f32,
}

impl Account for Ex {
    fn id(&self) -> i32 {
        self.account_id
    }
}

#[derive(PartialEq, Debug)]
struct Transaction<A> {
    from_id: i32,
    to_id: i32,
    amount: A,
}

impl ToUSDValue<GBP> for Ex {
    fn to_usd_value(&self, currency: GBP) -> f32 {
        (currency.0 as f32) * self.gbp
    }
}

impl FromUSDValue<CAD> for Ex {
    fn from_usd_value(&self, value: f32) -> CAD {
        CAD((value / self.cad) as i32)
    }
}

trait Account {
    fn id(&self) -> i32;
}

trait Exchange<F, T> {
    fn convert(&self, currency: F) -> T;
}

impl<E, F, T> Exchange<F, T> for E
where
    E: ToUSDValue<F> + FromUSDValue<T>,
{
    fn convert(&self, currency: F) -> T {
        self.from_usd_value(self.to_usd_value(currency))
    }
}

trait ExchangeAccount<F, T> {
    fn exchange(&self, from_id: i32, to_id: i32, amount: F) -> (Transaction<F>, Transaction<T>);
}

impl<E, F, T> ExchangeAccount<F, T> for E
where
    E: Exchange<F, T> + Account,
    F: Clone,
{
    fn exchange(&self, from_id: i32, to_id: i32, amount: F) -> (Transaction<F>, Transaction<T>) {
        let from_transaction = Transaction {
            from_id,
            to_id: self.id(),
            amount: amount.clone(),
        };
        let to_transaction = Transaction {
            from_id: self.id(),
            to_id,
            amount: self.convert(amount),
        };
        (from_transaction, to_transaction)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn exchange_works() {
        let gbp = GBP(200);
        let exchange = Ex {
            account_id: 0,
            cad: 0.7,
            gbp: 1.3,
        };
        let cad: CAD = exchange.convert(gbp);

        assert_eq!(cad, CAD(371));
    }

    #[test]
    fn transactions() {
        let ex = Ex {
            account_id: 30,
            cad: 0.7,
            gbp: 1.3,
        };
        let (from_transaction, to_transaction) = ex.exchange(20, 40, GBP(200));

        assert_eq!(
            from_transaction,
            Transaction {
                from_id: 20,
                to_id: 30,
                amount: GBP(200)
            }
        );
        assert_eq!(
            to_transaction,
            Transaction {
                from_id: 30,
                to_id: 40,
                amount: CAD(371)
            }
        );
    }
}
