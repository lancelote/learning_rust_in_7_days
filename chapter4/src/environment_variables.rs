#[allow(unused_imports)]
use std::env::{set_var, var};

#[allow(dead_code)]
fn road_len() -> usize {
    let env = var("ROAD").unwrap_or_else(|_| "".to_string());
    env.len()
}

#[allow(dead_code)]
fn rail_len() -> usize {
    let s = var("GWR").unwrap_or_else(|_| "".to_string());
    _rail_len(&s)
}

fn _rail_len(s: &str) -> usize {
    s.len()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn var_works() {
        let hello = var("HELLO").unwrap();
        assert_eq!(&hello, "WORLD");
    }

    #[test]
    fn road_len_works() {
        set_var("ROAD", "Route66");
        let length = road_len();
        assert_eq!(length, 7);
    }

    #[test]
    fn rail_len_works() {
        let r = _rail_len("Pointless Track");
        assert_eq!(r, 15);
    }
}
