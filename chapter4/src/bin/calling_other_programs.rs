use std::process::Command;

fn main() {
    let command = Command::new("ls")
        .arg("-l")
        .output()
        .expect("ls not usable");
    for (n, line) in String::from_utf8(command.stdout)
        .expect("not utf8")
        .split('\n')
        .enumerate()
    {
        println!("line {}: {}", n, line);
    }
}
