use std::io::copy;
use std::process::{Command, Stdio};

#[allow(unused_must_use)]
fn main() {
    let c1 = Command::new("wc")
        .stdin(Stdio::piped())
        .spawn()
        .expect("wc didn't run");
    let c2 = Command::new("ls")
        .stdout(Stdio::piped())
        .spawn()
        .expect("ls didn't run");

    copy(&mut c2.stdout.unwrap(), &mut c1.stdin.unwrap());
}
