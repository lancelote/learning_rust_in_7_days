use std::fs::File;
use std::io::{Read, Write};

#[allow(unused_must_use)]
fn main() -> std::io::Result<()> {
    let mut data = String::new();

    let mut from = File::open("README.md")?;
    from.read_to_string(&mut data);
    println!("{}", data);

    let mut to = File::create("RESULT.md")?;
    // copy(&mut from, &mut to)?;
    to.write_all(&data.into_bytes())?;

    Ok(())
}
