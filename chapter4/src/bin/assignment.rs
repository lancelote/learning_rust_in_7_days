use std::collections::BTreeMap;
use std::fs::File;
use std::io::Read;

fn adjust(accounts: &mut BTreeMap<String, f32>, name: &str, amount: f32) {
    match accounts.get_mut(name) {
        Some(current_value) => {
            *current_value += amount;
        }
        None => {
            accounts.insert(name.to_string(), amount);
        }
    }
}

fn main() {
    let mut accounts = BTreeMap::new();
    let mut file = File::open("data/payments.txt").unwrap();
    let mut data = String::new();
    file.read_to_string(&mut data).expect("failed to read file");

    for line in data.split('\n') {
        let entry: Vec<&str> = line.split(':').collect();
        println!("new entry: {:?}", entry);
        if entry.len() != 3 {
            println!("not 3 but {}", entry.len());
            continue;
        }

        let (currency, value) = entry[2].split_at(1);
        let value: f32 = value
            .parse()
            .unwrap_or_else(|_| panic!("failed to parse {} to i32", value));

        match currency {
            "$" => {
                adjust(&mut accounts, entry[0], -value);
                adjust(&mut accounts, entry[1], value);
            }
            _ => {
                println!("unknown currency: {}", currency);
            }
        }
    }

    println!("final accounts: {:?}", accounts);
}
