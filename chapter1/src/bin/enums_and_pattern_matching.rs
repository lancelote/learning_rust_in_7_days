#[derive(Debug)]
struct Bed {
    size: i32,
    count: u32,
}

#[derive(Debug)]
enum Room {
    Kitchen(i32),
    Bedroom(Bed),
    Lounge,
}

fn main() {
    use self::Room::*;
    let lounge = Lounge;
    println!("{:?}", lounge);

    let kitchen = Kitchen(4);
    println!("hello from {:?}", kitchen);

    let n = match kitchen {
        Room::Kitchen(n) => n,
        Room::Lounge => 0,
        Room::Bedroom(_) => 0,
    } + 1;

    println!("room {}", n);

    // only one case
    let bedroom = Bedroom(Bed { size: 1, count: 2 });
    if let Bedroom(n) = bedroom {
        println!("bedroom {:?}", n);
    }
}
