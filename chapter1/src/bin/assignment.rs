use std::env::args;

fn main() {
    for arg in args() {
        if arg.starts_with('W') {
            println!("hello {}", arg);
        }
    }
}
