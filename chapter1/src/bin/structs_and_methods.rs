#[derive(Debug)]
struct User {
    name: String,
    age: i32,
    height: i32,
    shoe_size: i32,
}

impl User {
    fn simple_string(&self) -> String {
        format!(
            "{} - {} - {}cm - shoe:{}",
            self.name, self.age, self.height, self.shoe_size
        )
    }

    fn grow(&mut self, diff: i32) {
        self.height += diff
    }

    fn die(self) {
        println!("dead {}", self.simple_string());
    }
}

fn main() {
    let mut user = User {
        name: String::from("Matt"),
        age: 33,
        height: 250,
        shoe_size: 10,
    };

    println!("{}", user.simple_string());
    user.grow(10);
    println!("{}", user.simple_string());
    user.die();
}
