use std::collections::HashMap;
use std::env::args;

fn main() {
    let mut hm = HashMap::new();
    hm.insert(3, "hello");
    hm.insert(5, "world");

    let hello = match hm.get(&3) {
        // a pointer is expected
        Some(value) => value,
        _ => "nothing",
    };
    println!("{}", hello);

    let world = hm.get(&5).unwrap();
    println!("{}", world);

    let nothing = hm.get(&4).unwrap_or(&"nothing");
    println!("{}", nothing);

    match "3".parse::<f32>() {
        Ok(v) => println!("Ok({})", v),
        Err(e) => println!("Err({})", e),
    }

    match get_arg(3) {
        Ok(v) => println!("Ok({})", v),
        Err(e) => println!("Err({})", e),
    }
}

fn get_arg(n: usize) -> Result<String, String> {
    for (i, arg) in args().enumerate() {
        if i == n {
            return Ok(arg);
        }
    }

    Err(String::from("not enough args"))
}
