fn main() {
    basic_loop();
    loop_for_in();
    while_loop();
    array_loop();
}

fn basic_loop() {
    println!("\nbasic loop\n");

    let mut n = 0;
    loop {
        n += 1;
        println!("hello");
        if n >= 10 {
            return;
        }
    }
}

fn loop_for_in() {
    println!("\nfor loop\n");

    for n in 0..10 {
        println!("tick {}", n);
    }
}

fn while_loop() {
    println!("\nwhile loop\n");

    let mut n = 0;
    while n < 10 {
        n += 1;
        println!("tick {}", n);
    }
}

fn array_loop() {
    println!("\niterate over an array\n");

    let mut v = Vec::new();
    v.push(4);
    v.push(7);
    v.push(9);

    'outer: for _i in 0..10 {
        for n in &v {
            println!("item {}", n);
            if n == &7 {
                break 'outer;
            }
        }
    }
}
