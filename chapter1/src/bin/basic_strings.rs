fn main() {
    let s = String::from("hello 中国");

    println!("count l's: {}\n", count_l(&s));

    for (i, c) in s.chars().enumerate() {
        println!("{}: {}", i, c);
    }

    println!("\nbytes:");
    for b in s.bytes() {
        println!("{}", b);
    }
}

fn count_l(s: &str) -> i32 {
    let mut res = 0;

    for c in s.chars() {
        if c == 'l' {
            res += 1;
        }
    }

    res
}
