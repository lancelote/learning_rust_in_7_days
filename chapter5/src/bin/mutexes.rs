use std::sync::{Arc, Mutex};
use std::thread::spawn;

fn main() {
    let mutex = Arc::new(Mutex::new(42));
    let mut handles = Vec::with_capacity(10);

    for i in 0..10 {
        let mutex_copy = mutex.clone();
        let j = i;

        handles.push(spawn(move || {
            let mut guard = mutex_copy.lock().unwrap();
            *guard += j;
            println!("j = {}, guard = {}", j, *guard);
        }));
    }

    for handler in handles {
        handler.join().unwrap();
    }

    println!("done");
}
