use rayon::prelude::*;
use std::thread::sleep;
use std::time::Duration;

fn main() {
    let mut v = Vec::with_capacity(500);
    for i in 0..500 {
        v.push(i);
    }

    let v2: Vec<i32> = (&v)
        .par_iter()
        .map(|&x| {
            sleep(Duration::from_millis(100));
            println!("{}", x);
            x * x
        })
        .collect();

    println!("{:?}", &v2[490..500]);
}
