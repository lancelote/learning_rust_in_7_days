use std::sync::mpsc::{channel, Sender};
use std::sync::{Arc, Mutex};
use std::thread::{spawn, JoinHandle};

trait FnBox {
    fn call_box(self: Box<Self>);
}

impl<F: FnOnce()> FnBox for F {
    fn call_box(self: Box<Self>) {
        (*self)();
    }
}

type Job = Box<dyn FnBox + 'static + Send>;

pub struct ThreadPool {
    handles: Vec<JoinHandle<()>>,
    sender: Sender<Job>,
}

impl ThreadPool {
    pub fn new(n: usize) -> Self {
        let (sender, receiver) = channel::<Job>();
        let amap = Arc::new(Mutex::new(receiver));
        let mut handlers = Vec::with_capacity(n);

        for _ in 0..n {
            let amp = amap.clone();
            handlers.push(spawn(move || loop {
                let job = match amp.lock().unwrap().recv() {
                    Ok(j) => j,
                    _ => return,
                };
                job.call_box();
            }));
        }
        ThreadPool {
            handles: handlers,
            sender,
        }
    }

    pub fn add<F: FnOnce() + 'static + Send>(&self, f: F) {
        self.sender.send(Box::new(f)).unwrap();
    }

    pub fn end(self) {
        drop(self.sender);
        for handle in self.handles {
            handle.join().unwrap();
        }
    }
}

fn main() {
    let pool = ThreadPool::new(10);
    pool.add(|| {
        println!("hello");
    });
    pool.end();
    println!("done");
}
