use std::thread::{sleep, spawn};
use std::time::Duration;

fn main() {
    let thread = spawn(|| {
        for i in 0..10 {
            sleep(Duration::from_millis(10));
            println!("{}", i);
        }

        42
    });

    for i in 10..20 {
        sleep(Duration::from_millis(10));
        println!("{}", i);
    }

    let result = thread.join().unwrap();
    println!("done, result = {}", result);
}
