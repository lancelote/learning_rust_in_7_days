use std::sync::mpsc;
use std::thread::spawn;

fn main() {
    let (sender, receiver) = mpsc::channel::<i32>();

    let thread = spawn(move || loop {
        match receiver.recv() {
            Ok(value) => {
                println!("value = {}", value);
            }
            Err(error) => {
                println!("error = {:?}", error);
                return;
            }
        }
    });

    let sender2 = sender.clone();

    spawn(move || {
        for j in 10..20 {
            sender2.send(j).unwrap();
        }
    });

    for i in 0..10 {
        sender.send(i).expect("receiver dropped early");
    }
    drop(sender); // to close the channel
    thread.join().unwrap();
}
