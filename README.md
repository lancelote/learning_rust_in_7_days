# learning_rust_in_7_days

Code from "Learn Rust in 7 Days" video course by Matthew Stoodley

## TOC

- [x] Chapter 1: Getting Started with Basic Tools and Syntax
    - [Basic Syntax](chapter1/src/bin/syntax.rs)
    - [Basic Loops](chapter1/src/bin/loops.rs)
    - [Basic Strings](chapter1/src/bin/basic_strings.rs)
    - [Structs and Methods](chapter1/src/bin/structs_and_methods.rs)
    - [Enums and Pattern Matching](chapter1/src/bin/enums_and_pattern_matching.rs)
    - [Result and Option](chapter1/src/bin/result_and_option.rs)
    - [Assignment](chapter1/src/bin/assignment.rs)
- [x] Chapter 2: Traits
    - [Introduction to Traits](chapter2/src/bin/introduction_to_traits.rs)
    - [Using Libraries](chapter2/src/bin/libraries.rs)
    - [Defining a Trait](chapter2/src/defining_a_trait.rs)
    - [Accepting Generic Parameters](chapter2/src/accepting_generic_parameters.rs)
    - [Generic Structs](chapter2/src/generic_structs.rs)
    - [Generic Iterators](chapter2/src/generic_iterator.rs)
    - [Useful Traits](chapter2/src/useful_traits.rs)
    - [Error Handling with Traits](chapter2/src/error_handling_with_traits.rs)
- [x] Chapter 3: Lifetimes
    - [Pointer](chapter3/src/lifetimes.rs)
    - [Passing a Borrow Forward](chapter3/src/passing_a_borrow_forward.rs)
    - [Stack vs. Heap](chapter3/src/stack_vs_heap.rs)
    - [Static Lifetimes](chapter3/src/static_lifetimes.rs)
    - [Reference Counting](chapter3/src/reference_counting.rs)
    - [Assignment - Binary Tree](chapter3/src/assignment.rs)
- [x] Chapter 4: The Program Environment
    - [Environment Variables](chapter4/src/environment_variables.rs)
    - [Calling Other Programs](chapter4/src/bin/calling_other_programs.rs)
    - [Piping between Other Programs](chapter4/src/bin/piping_between_other_programs.rs)
    - [File Access](chapter4/src/bin/file_access.rs)
    - [Assignment - Files](chapter4/src/bin/assignment.rs)
- [x] Chapter 5: Threads and Channels
    - [Threads](chapter5/src/bin/threads.rs)
    - [Channels](chapter5/src/bin/channels.rs)
    - [Mutexes](chapter5/src/bin/mutexes.rs)
    - [Thread Pools and Workers](chapter5/src/bin/thread_pools_and_workers.rs)
    - [Rayon for Parallel Problems](chapter5/src/bin/rayon_example.rs)
    - [Assignment - Bank](chapter5/src/bin/assignment.rs)
- [ ] Chapter 6: Databases
- [ ] Chapter 7: Building Our Database into an Online Banks